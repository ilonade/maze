<link href="pdf.css" rel="stylesheet">

<div class="front-page">

<h1>Mobile Game “Maze”<br>With Accelerometer Controls</h1>
<h2>Documentation</h2>

<p>
<br>
<em>Ilona Demidenko<br>Melina Hortz</em>
</p>

</div>

<div class="page-break"></div>

## Introduction

Maze games on mobile are not fun if you can see the whole map. What if we spiced it up to involve some skill-based gameplay, where one wrong move can get you a “Game Over?”

That is what this report is about.

We have set out to build a mobile game “Maze” where the player controls the ball with the accelerometer – tilting the mobile device in different directions. And it is very easy to get a “Game Over” thanks to holes in the ground of the maze.

## Formulation of The Problem

Build a mobile game “Maze” for Android 4.1+.

In the game, there should be multiple levels. Each level should contain:

- Exactly one ball.
- Exactly one escape point.
- Several walls.
- Several holes.

The rules of the game are simple:

- When player tilts their mobile device in particular direction – the ball should start moving in the same direction, and should realistically accelerate as if there is proper earth gravity force pulling on it.
- The ball should not escape the rectangular borders of the level.
- The ball should not pass through walls.
- The ball should bounce of walls and borders on collision.
- When the ball collides too much with a hole, the game should be over, and the player should see a “Game Over” screen.
- On the “Game Over” screen the player can restart the level.
- When the ball collides too much with an escape point, the level should be complete, and the player should see a “Level Complete” screen.
- On the “Level Complete” screen the player can advance to the next level or restart the current level.
- When the player completes the last level, the game should be complete, and the player should see “Game Won” screen.
- On the “Game Won” screen the player should be able to restart the game again or restart the current level.

It should be simple to create and change existing levels. Therefore it would be nice to have levels described in some form of DSL.

## Architecture of The Solution

We have used Entity-Component-System (ECS) architecture. That is an architecture most often used by games and simulations.

**Components** are the most basic units of data of the system. Usually, components, contain only one or two values, and rarely, more. Sometimes, components don’t have any data, and are only characterized by their presence or not. For example:

- `Position(x: Float, y: Float)`
- `Velocity(x: Float, y: Float)`
- `Name(name: String)`
- `BounceOnCollision()`
- `Victory(reason: String)`

Then there are **entities**. They are bags of components. Certain entity can have some components present. For example, the ball entity in the game has the following:

```kotlin
name("ball")
color(0xffe5e5e5)
position(15.0f, 25.0f)
circleShape(3.0f)
velocity(0.0f, 0.0f, max = 30.0f)
acceleration(0.0f, 0.0f, max = 10.0f)
mass(0.1f)
force(0.0f, 0.0f, max = 100.0f)
accelerometerControl()
bounceOnCollision()
markedOnCollision()
```

The entity can have only a single component of any given type.

Components and entities so far don’t have any behavior. Components are just chunks of data, and entities are collections that tie these data clumps together in a meaningful “thing.”

In ECS all the behavior goes into **systems**.

Every system has a very small responsibility. You could say every system should follow a Single-Responsibility Principle from OOP.

For example, `VelocitySystem` will be responsible only for changing `Position` components’ data according to data that resides in the corresponding `Velocity` components.

And such correspondence is determined by entities. So, roughly such system will work in the following way:

1. Iterate over all `Velocity` components in the **world** (world contains all the components that are present in the game) and for each such component:
    1. Obtain the entity which holds this component.
    2. Check if that entity has `Position` component:
        1. If yes: increase `x` and `y` coordinates of `Position` component by corresponding `x` and `y` vector parts of `Velocity` component.
        2. If no: ignore this `Velocity` component and move on to the next one.

```kotlin
velocities.forEach { velocity ->
    // attempt to obtain entity which holds velocity
    val entity = velocity.entity
            ?: return@forEach

    // attempt to obtain position from entity
    val position = entity.get<Position>() 
            ?: return@forEach

    // increase coordinates appropriately
    // VELOCITY_RATIO is a multiplier for frame rate
    position.x += velocity.x / VELOCITY_RATIO
    position.y += velocity.y / VELOCITY_RATIO
}
```

Every system has multiple methods that it can implement, such as:

- `update()` – executed on every frame. Should do any data updates in the world. For example `VictorySystem`.
- `render()` – executed on every frame after all `update`s are done. Should do any rendering on the screen. For example `CircleShapeRenderingSystem`.
- `updateAccelerometer(x, y, z)` – executed on every accelerometer sensor update. Should make any adjustments needed to the data of the world when the player changes the tilt of the mobile device. For example `AccelerometerControlSystem`.
- `renderDialog(dialogService)` – executed on every frame after all `update`s and `render`s are done. Should do any Android dialog rendering via the provided `dialogService` object. For example: `DialogSystem`.
- `activateDialogButtonAction(dialog, button)` – executed whenever the player taps on one of the dialog buttons. Should execute the corresponding dialog button action and change the state of the world accordingly. For example `DialogSystem` creates a new entity with a single component that is specified in the `button`’s `action` field. That will trigger other systems in the next frame.

The whole architecture depends on the fact that systems are all being called in predictable order every single frame.

That way system A can change the data in the world and add some new entity. Then system B can pick it up and do its processing and enrich the world with more data, and so on.

That is a Single-Responsibility Principle almost taken to its absolute. You can encode every game rule as a separate system, or even as a collection of systems working together and independently of each other.

The only dependency every system has – is the structure of the data. These are components and entities.

That is a drawback of ECS. Once created and used it is very hard to change the structure of the components and entities.

Fortunately, you could just create new ones, and slowly transition the codebase to use new components instead of old ones.

Alright, so we can implement the game with ECS architecture now. We can encode any game rule in these systems.

What about the DSL for creating the levels?

We decided to create levels by creating the entities with components inside of them. For example:

```kotlin
val ball = new Entity()
ball.set(Position(15.0f, 25.0f))
ball.set(Radius(3.0f))
// …
ball.set(BounceOnCollision())

val wallA = new Entity()
// … and 30 more components like that per level …
```

Now we could have removed a bit of boilerplate by:

1. Removing the need to instantiate the component and refer to a variable using the Kotlin’s blocks with the receiver:

   ```kotlin
   Entity.create {
       set(Position(15.0f, 25.0f))
       set(Radius(3.0f))
       // …
       set(BounceOnCollision())
   }
   ```
   
2. Removing the need to call `set(…)` every time by providing a special function for every component that we have:

   ```kotlin
   Entity.create {
       position(15.0f, 25.0f)
       radius(3.0f)
       // …
       bounceOnCollision()
   }
   ```

3. Then take all the common entities that we need to create for every level and turn them into the functions where parameters are the only things that change:

   ```kotlin
   ball(pos = 15 x 25, radius = 3.0f)
   wall(pos = 30 x 10, size = 5 x 20)
   hole(pos = 75 x 35, radius = 3.5f)
   escapePoint(pos = 90 x 90, radius = 3.5f)
   ```

4. Finally, take all the common entities that define a level (level metadata, maze floor, level boundaries) and provide a common function to create all of these:

   ```kotlin
   createLevel(
           name = "Level 1",
           self = levelOne,
           next = levelTwo,
           size = 40 x 40
   )
   ```

That way, the level can be described quite easily and here is an example of a single level:

```kotlin
class LevelOne : LevelCreator {
    override fun create() {
        createLevel(
                name = "Level 1",
                self = levelOne,
                next = levelTwo,
                size = 40 x 40
        )

        // b w . . . w . .
        // . w . w . w . .
        // . w . w . w . .
        // . w . w . . . .
        // . w . w h w . .
        // . w . w . w . .
        // . . . w . w . E
        // . w . . . w . .

        ball(0 x 0, radius = 2f)

        escapePoint(35 x 30, radius = 2.5f)

        hole(20 x 20, radius = 2.5f)

        wall(pos = 5 x 0, size = 5 x 30)
        wall(pos = 5 x 35, size = 5 x 5)
        wall(pos = 15 x 5, size = 5 x 30)
        wall(pos = 25 x 0, size = 5 x 15)
        wall(pos = 25 x 20, size = 5 x 20)
    }
}

val levelOne = LevelOne()
```

Since the level definition consists fully of components and entities, there is a potential for creating a visual level editor without changing how levels are described. But this is going to be out of the scope of the project.

The final piece of ECS architecture is the main loop. Or `Game` class.

`Game` class is running a single frame 60 times per second (the usual frame rate that the player would expect). For each frame, it is going to call all the `update`, `render` (and so on) functions on every single system that is defined.

Also, `Game` class serves as a facade for interaction with Android framework. So any event that application receives is received by a specific delegate method of `Game` class. Then it transforms it into what systems can understand (for example `activateDialogButtonAction` and `updateAccelerometer`) and delivers that message to all systems.

## User Interface

Unfortunately, the application doesn’t have the title screen. Instead the player is immediately thrown into the action—the first level:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/first-level.png)

You can see here:

- White circle shape – ball.
- Black circle shape – hole.
- Green circle shape – escape point.
- Dark brown rectangles – walls.
- Gray surface – traversable ground of the maze.

If the player loses the game by dropping a ball into the hole, the game over screen pops up:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/game-over.png)

The only option “Retry” will restart the current level.

When player finally reaches the destination—escape point, the level complete screen pops up:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/level-complete.png)

Now if the player taps the “Retry level” option—the current level will be restarted.

If the player taps the “Next level” option—the game will advance, and the next level will begin.

Finally, when the player finishes the last level of the game the following screen will pop out:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/game-complete.png)

“Retry level” does the same as in the previous screen.

“New Game” option is going to restart the whole game from the level one.

## Class Diagrams

Since the architecture is not the usual Object-Oriented one, the class diagrams are not that interesting, and not that useful. We are going to provide them here anyway, but if you are curious about that, better read the “Architecture” section of this report.

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram-high-overview-.png)

Here are the base component classes and interfaces:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram-component-related-stuff.png)

And here are all of the specific components of this application:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-components--01-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-components--02-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-components--03-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-components--04-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-components--05-.png)

And here are all of the systems:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-systems--01-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-systems--02-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-systems--03-.png)

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--all-systems--04-.png)

And here is the glue that makes the application tick, like Android activity and view, like main `Game` object, and `Entity` that ties several components together:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--glue-.png)

Then there are few bridge classes that make all the game logic code independent from Android platform, such as `Renderer` (and its `RenderCommand`s), and `DialogService`:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--renderer-and-dialog-service-.png)

Then there are DSL functions that help us reduce boilerplate when creating new levels:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--level-helpers-.png)

Speaking of levels… All the levels are implementing `LevelCreator` interface, take a look:

<div class="center-it img-ok-to-be-bigger"></div>
![](./img/class-diagram--levels-.png)

Find the full class diagram here (not sliced in different sections, and all the details included):

https://bitbucket.org/ilonade/maze/src/master/docs/img/full-class-diagram-for-the-brave.png

## Conclusion

We have attained the goal of creating a mobile maze game that is not boring, challenging, and requires skill to play.

We have fully completed tasks from the section “Formulation of The Problem.”

The user interface can be improved in the future:

- Title screen can be added.
- “Level complete,” “Game over,” and “Game beaten” screens can be improved using proper views instead of built-in dialog alerts.
- Game graphics can be improved.

We have learned how to:

- Apply ECS architecture to game development for mobile devices.
- Create full-screen applications on Android.
- Work with accelerometer sensor on Android.

## Who Did What

### Ilona Demidenko

- Coding:
    - Architecture design
    - ECS architecture implementation
    - Game logic in the form of components and systems
- Presentation:
    - Architecture of the Application
    - What was done and TODO
- Report:
    - Architecture of The Solution
    - Class Diagrams


### Melina Hortz

- Coding:
    - Game levels design
    - Connection of game logic with Android ecosystem (`DialogService`, `Renderer`, `GameView`, etc.)
- Presentation:
    - Description of the game
    - Screenshots
- Report:
    - Introduction
    - User Interfaces
    - Conclusion
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Velocity

class VelocityResistanceSystem : System {

    companion object {
        private val resistance = 0.995f
    }

    override fun update() {
        Velocity.velocities.forEach { velocity ->
            velocity.x *= resistance
            velocity.y *= resistance
            velocity.cap()
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.AdvanceToNextLevel.Companion.advancesToNextLevel
import com.ilonacodes.maze.components.NextLevel.Companion.nextLevels

class AdvanceToNextLevelSystem : System {

    override fun update() {
        advancesToNextLevel.first() ?: return
        val level = nextLevels.first() ?: return

        level.creator.create()
    }

}
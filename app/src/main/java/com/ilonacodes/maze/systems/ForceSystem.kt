package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Acceleration
import com.ilonacodes.maze.components.Force.Companion.forces
import com.ilonacodes.maze.components.Mass

class ForceSystem : System {

    override fun update() {
        forces.forEach { force ->
            val entity = force.entity ?: return@forEach
            val mass = entity.get<Mass>() ?: return@forEach
            val acceleration = entity.get<Acceleration>() ?: return@forEach

            acceleration.x = force.x / mass.mass
            acceleration.y = force.y / mass.mass
            acceleration.cap()
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.*
import com.ilonacodes.maze.components.Level.Companion.levels
import com.ilonacodes.maze.components.Victory.Companion.victories

class VictorySystem : System {

    private val nextLevel = DialogButton(
            title = "Next level",
            action = { set(AdvanceToNextLevel()) }
    )

    private val retryLevel = DialogButton(
            title = "Retry level",
            action = { set(RetryLevel()) }
    )

    private val newGame = DialogButton(
            title = "New Game",
            action = { set(NewGame()) }
    )

    override fun update() {
        victories.forEach { victory ->
            when {
                gameBeaten() -> dialogForBeatingTheGame(victory)
                else -> dialogForBeatingALevel(victory)
            }
        }

        victories.asList().forEach { it.entity?.removeEntity() }
    }

    private fun gameBeaten() = levels.firstEntity()?.get<NextLevel>() == null

    private fun dialogForBeatingALevel(victory: Victory) {
        Entity.create {
            dialog(title = "LEVEL COMPLETE",
                    text = victory.reason,
                    buttons = listOf(nextLevel, retryLevel))
        }
    }

    private fun dialogForBeatingTheGame(victory: Victory) {
        Entity.create {
            dialog(title = "GAME BEATEN",
                    text = victory.reason,
                    buttons = listOf(newGame, retryLevel))
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Arrow
import com.ilonacodes.maze.components.Arrow.Companion.arrows


class AccelerometerDebugSystem : System {

    init {
        arrows.add(Arrow(
                startX = 0f,
                startY = 0f,
                endX = -50f,
                endY = 0f,
                color = 0xff00ff00))

        arrows.add(Arrow(
                startX = 0f,
                startY = 0f,
                endX = 0f,
                endY = 50f,
                color = 0xffff0000))

        arrows.add(Arrow(
                startX = 0f,
                startY = 0f,
                endX = 35f,
                endY = -35f,
                color = 0xff0000ff))
    }

    override fun update() {

    }

    override fun updateAccelerometer(x: Float, y: Float, z: Float) {
        arrows[0].endX = -x * 10

        arrows[1].endY = y * 10

        arrows[2].endX = z / 1.41f * 10
        arrows[2].endY = -z / 1.41f * 10
    }

}
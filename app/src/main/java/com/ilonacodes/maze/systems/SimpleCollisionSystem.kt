package com.ilonacodes.maze.systems

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.System
import com.ilonacodes.maze.VELOCITY_RATIO
import com.ilonacodes.maze.components.*
import com.ilonacodes.maze.components.BounceOnCollision.Companion.bouncesOnCollision
import com.ilonacodes.maze.components.MarkOnCollision.Companion.marksOnCollision
import com.ilonacodes.maze.components.MarkedOnCollision.Companion.markedsOnCollision

class SimpleCollisionSystem : System {

    companion object {
        private val bumpiness = 1.4f
        private val eps = 1e-6
        private val intersectRadiusThreshold = 1.5f
    }

    override fun update() {

        bouncesOnCollision.forEachEntity { first ->
            handlePossibleBounce(first)
        }

        marksOnCollision.forEachEntity { first ->
            handlePossibleMark(first)
        }

    }

    private fun handlePossibleBounce(first: Entity) {
        bouncesOnCollision.forEachEntity { second ->
            val circleShape = first.get<CircleShape>() ?: return@forEachEntity
            val velocityValue = first.get<Velocity>() ?: return@forEachEntity
            val velocity = Velocity(
                    x = velocityValue.x / VELOCITY_RATIO,
                    y = velocityValue.y / VELOCITY_RATIO,
                    max = velocityValue.max
            )
            val firstPosition = first.get<Position>() ?: return@forEachEntity

            val rectangleShape = second.get<RectangleShape>() ?: return@forEachEntity
            val secondPosition = second.get<Position>() ?: return@forEachEntity

            val intersectVector = intersectCircleAndRectangle(
                    firstPosition, circleShape, secondPosition, rectangleShape)
                    ?: return@forEachEntity

            val projection = intersectVector.x * velocity.x + intersectVector.y * velocity.y
            intersectVector.x *= projection
            intersectVector.y *= projection

            val pushedAwayCandidate = Position(
                    x = firstPosition.x - intersectVector.x * bumpiness,
                    y = firstPosition.y - intersectVector.y * bumpiness)

            if (intersectCircleAndRectangle(pushedAwayCandidate, circleShape, secondPosition, rectangleShape) != null) {

                val pushedAway = findPositionJustBeforeIntersection(
                        firstPosition,
                        circleShape,
                        velocity,
                        secondPosition,
                        rectangleShape)

                if (pushedAway != null) {
                    firstPosition.x = pushedAway.x
                    firstPosition.y = pushedAway.y
                } else {
                    firstPosition.x = pushedAwayCandidate.x
                    firstPosition.y = pushedAwayCandidate.y
                }
            } else {
                firstPosition.x = pushedAwayCandidate.x
                firstPosition.y = pushedAwayCandidate.y
            }

            velocity.x -= intersectVector.x * bumpiness
            velocity.y -= intersectVector.y * bumpiness

            velocityValue.x = velocity.x * VELOCITY_RATIO
            velocityValue.y = velocity.y * VELOCITY_RATIO
            velocityValue.cap()

        }
    }

    private fun handlePossibleMark(first: Entity) {
        markedsOnCollision.forEachEntity { second ->
            val firstMark = first.get<MarkOnCollision>() ?: return@forEachEntity
            val firstPosition = first.get<Position>() ?: return@forEachEntity
            val firstShape = first.get<CircleShape>() ?: return@forEachEntity

            val secondPosition = second.get<Position>() ?: return@forEachEntity
            val secondShape = second.get<CircleShape>() ?: return@forEachEntity

            val intersectRadius = intersectCircleAndCircle(
                    firstPosition, firstShape, secondPosition, secondShape)

            if (intersectRadius > intersectRadiusThreshold) {
                firstMark.mark?.invoke(second)
            }
        }
    }

    private fun intersectCircleAndCircle(firstPosition: Position,
                                         firstShape: CircleShape,
                                         secondPosition: Position,
                                         secondShape: CircleShape): Float {

        val distanceBetweenCenters = distSqr(firstPosition, secondPosition)
        val compoundRadius = firstShape.radius + secondShape.radius
        return compoundRadius - distanceBetweenCenters

    }

    private fun findPositionJustBeforeIntersection(firstPosition: Position,
                                                   circleShape: CircleShape,
                                                   velocity: Velocity,
                                                   secondPosition: Position,
                                                   rectangleShape: RectangleShape): Position? {

        return (1 until 30)
                .map {
                    Position(
                            x = firstPosition.x - velocity.x / 10 * it,
                            y = firstPosition.y - velocity.y / 10 * it
                    )
                }
                .firstOrNull { intersectCircleAndRectangle(it, circleShape, secondPosition, rectangleShape) == null }

    }

    private fun intersectCircleAndRectangle(firstPosition: Position,
                                            circleShape: CircleShape,
                                            secondPosition: Position,
                                            rectangleShape: RectangleShape): Position? {

        fun possibleIntersection(point: Position, start: Position, end: Position): Position {
            val lengthSqr = distSqr(start, end)

            if (lengthSqr < eps) return start

            var projected = ((point.x - start.x) * (end.x - start.x) +
                    (point.y - start.y) * (end.y - start.y)) /
                    lengthSqr

            projected = maxOf(0f, minOf(1f, projected))

            return Position(
                    x = start.x + projected * (end.x - start.x),
                    y = start.y + projected * (end.y - start.y)
            )
        }

        fun isInCircle(point: Position): Position? {
            val distance = distSqr(point, firstPosition)

            if (distance <= circleShape.radius.sqr()) {
                val realDistance = distance.sqrt()

                return Position(
                        x = (firstPosition.x - point.x) / realDistance,
                        y = (firstPosition.y - point.y) / realDistance
                )
            }

            return null
        }

        val leftTop = secondPosition
        val rightTop = Position(
                x = secondPosition.x + rectangleShape.width,
                y = secondPosition.y)
        val leftBottom = Position(
                x = secondPosition.x,
                y = secondPosition.y + rectangleShape.height)
        val rightBottom = Position(
                x = secondPosition.x + rectangleShape.width,
                y = secondPosition.y + rectangleShape.height)


        isInCircle(possibleIntersection(firstPosition, leftTop, rightTop))?.let { return it }
        isInCircle(possibleIntersection(firstPosition, leftTop, leftBottom))?.let { return it }
        isInCircle(possibleIntersection(firstPosition, rightTop, rightBottom))?.let { return it }
        isInCircle(possibleIntersection(firstPosition, leftBottom, rightBottom))?.let { return it }

        return null
    }

    private fun Float.sqr(): Float {
        return this * this
    }

    private fun Float.sqrt(): Float {
        return Math.sqrt(this.toDouble()).toFloat()
    }

    private fun distSqr(first: Position, second: Position): Float {
        return (first.x - second.x).sqr() + (first.y - second.y).sqr()
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Level.Companion.levels
import com.ilonacodes.maze.components.RetryLevel.Companion.retryLevels

class RetryLevelSystem : System {

    override fun update() {
        retryLevels.first() ?: return
        val level = levels.first() ?: return

        retryLevels.clearEntities()

        level.creator.create()
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Defeat.Companion.defeats
import com.ilonacodes.maze.components.DialogButton
import com.ilonacodes.maze.components.RetryLevel
import com.ilonacodes.maze.components.dialog

class DefeatSystem : System {

    private val tryAgain = DialogButton(
            title = "Retry",
            action = { set(RetryLevel()) }
    )

    override fun update() {
        defeats.forEach { defeat ->
            Entity.create {
                dialog(title = "DEFEAT",
                        text = defeat.reason,
                        buttons = listOf(tryAgain))
            }
        }

        defeats.asList().forEach { it.entity?.removeEntity() }
    }

}
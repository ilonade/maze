package com.ilonacodes.maze.systems

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Dialog
import com.ilonacodes.maze.components.Dialog.Companion.dialogs
import com.ilonacodes.maze.components.DialogButton
import com.ilonacodes.maze.dialog.DialogService

class DialogSystem : System {

    override fun renderDialog(dialogService: DialogService) {
        dialogs.forEach { dialog ->
            dialogService.addDialog(dialog)
        }

        dialogs.asList().forEach { it.entity?.removeEntity() }
    }

    override fun activateDialogButtonAction(dialog: Dialog, button: DialogButton) {
        Entity.create {
            button.action(this)
        }
    }

}
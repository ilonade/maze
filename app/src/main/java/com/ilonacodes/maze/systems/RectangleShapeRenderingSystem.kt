package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Color
import com.ilonacodes.maze.components.Position
import com.ilonacodes.maze.components.RectangleShape.Companion.rectangleShapes
import com.ilonacodes.maze.components.ViewPort
import com.ilonacodes.maze.renderer.Renderer
import com.ilonacodes.maze.renderer.commands.Rectangle

class RectangleShapeRenderingSystem : System {

    override fun render(renderer: Renderer) {
        val view = ViewPort.instance

        rectangleShapes.forEach { rectangleShape ->
            val entity = rectangleShape.entity ?: return@forEach
            val color = entity.get<Color>() ?: return@forEach
            val position = entity.get<Position>() ?: return@forEach

            val command = Rectangle(
                    position.x * view.scale + view.originX,
                    position.y * view.scale + view.originY,
                    rectangleShape.width * view.scale,
                    rectangleShape.height * view.scale,
                    color.color)

            renderer.add(command)
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.VELOCITY_RATIO
import com.ilonacodes.maze.components.Position
import com.ilonacodes.maze.components.Velocity.Companion.velocities

class VelocitySystem : System {

    override fun update() {
        velocities.forEach { velocity ->
            val entity = velocity.entity ?: return@forEach
            val position = entity.get<Position>() ?: return@forEach

            position.x += velocity.x / VELOCITY_RATIO
            position.y += velocity.y / VELOCITY_RATIO
        }
    }

}
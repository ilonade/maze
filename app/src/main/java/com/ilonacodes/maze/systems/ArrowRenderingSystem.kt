package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Arrow.Companion.arrows
import com.ilonacodes.maze.renderer.Renderer
import com.ilonacodes.maze.renderer.commands.Line

class ArrowRenderingSystem : System {

    override fun render(renderer: Renderer) {

        arrows.forEach {
            renderer.add(Line(
                    it.startX, it.startY,
                    it.endX, it.endY,
                    it.color))

            val vecX = (it.endX - it.startX)
            val vecY = (it.endY - it.startY)

            val vecSize = Math.sqrt(vecX * vecX + vecY * vecY.toDouble()).toFloat()

            val normX = 5 * vecX / vecSize
            val normY = 5 * vecY / vecSize

            renderer.add(Line(
                    it.endX,
                    it.endY,
                    it.endX - normX - normY,
                    it.endY - normY + normX,
                    it.color))

            renderer.add(Line(
                    it.endX,
                    it.endY,
                    it.endX - normX + normY,
                    it.endY - normY - normX,
                    it.color))
        }

    }

}
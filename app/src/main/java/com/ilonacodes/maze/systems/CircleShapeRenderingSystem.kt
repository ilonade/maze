package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.CircleShape.Companion.circleShapes
import com.ilonacodes.maze.components.Color
import com.ilonacodes.maze.components.Position
import com.ilonacodes.maze.components.ViewPort
import com.ilonacodes.maze.renderer.Renderer
import com.ilonacodes.maze.renderer.commands.Circle

class CircleShapeRenderingSystem : System {

    override fun render(renderer: Renderer) {
        val view = ViewPort.instance

        circleShapes.forEach { circleShape ->
            val entity = circleShape.entity ?: return@forEach
            val color = entity.get<Color>() ?: return@forEach
            val position = entity.get<Position>() ?: return@forEach

            val command = Circle(
                    position.x * view.scale + view.originX,
                    position.y * view.scale + view.originY,
                    circleShape.radius * view.scale,
                    color.color)

            renderer.add(command)
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.ACCELERATION_RATIO
import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.Acceleration.Companion.accelerations
import com.ilonacodes.maze.components.Velocity

class AccelerationSystem : System {

    override fun update() {
        accelerations.forEach { acceleration ->
            val entity = acceleration.entity ?: return@forEach
            val velocity = entity.get<Velocity>() ?: return@forEach

            velocity.x += acceleration.x / ACCELERATION_RATIO
            velocity.y += acceleration.y / ACCELERATION_RATIO
            velocity.cap()
        }
    }

}
package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.ViewPort.Companion.viewPorts
import com.ilonacodes.maze.renderer.Renderer
import com.ilonacodes.maze.renderer.commands.InitViewPortHack

class ViewPortSystem : System {

    override fun render(renderer: Renderer) {
        renderer.add(InitViewPortHack())
    }

    override fun update() {
        viewPorts.forEach {
            val xScale = it.screenWidth / it.width
            val yScale = it.screenHeight / it.height

            it.scale = minOf(xScale, yScale)
            it.originX = it.screenWidth / 2 - (it.x + it.width / 2) * it.scale
            it.originY = it.screenHeight / 2 - (it.y + it.height / 2) * it.scale
        }
    }

}
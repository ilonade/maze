package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.FinalLevel.Companion.finalLevels
import com.ilonacodes.maze.components.NewGame.Companion.newGames

class NewGameSystem : System {

    override fun update() {
        newGames.first() ?: return
        val finalLevel = finalLevels.first() ?: return

        newGames.clearEntities()

        finalLevel.newGame.create()
    }

}
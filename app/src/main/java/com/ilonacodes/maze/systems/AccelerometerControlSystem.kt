package com.ilonacodes.maze.systems

import com.ilonacodes.maze.System
import com.ilonacodes.maze.components.AccelerometerControl.Companion.accelerometerControls
import com.ilonacodes.maze.components.Force
import com.ilonacodes.maze.components.Mass

class AccelerometerControlSystem : System {

    companion object {
        private val threshold = 1f
    }

    override fun updateAccelerometer(x: Float, y: Float, z: Float) {
        accelerometerControls.forEach {
            val entity = it.entity ?: return@forEach
            val force = entity.get<Force>() ?: return@forEach
            val mass = entity.get<Mass>() ?: return@forEach

            force.x = meaningfulForceComponent(-x) * mass.mass
            force.y = meaningfulForceComponent(y) * mass.mass
            force.cap()
        }
    }

    private fun meaningfulForceComponent(x: Float) =
            if (Math.abs(x) > threshold) x
            else 0f

}
package com.ilonacodes.maze

import com.ilonacodes.maze.components.Dialog
import com.ilonacodes.maze.components.DialogButton
import com.ilonacodes.maze.dialog.DialogService
import com.ilonacodes.maze.renderer.Renderer

interface System {

    fun update() {}
    fun updateAccelerometer(x: Float, y: Float, z: Float) {}
    fun render(renderer: Renderer) {}

    fun activateDialogButtonAction(dialog: Dialog, button: DialogButton) {}
    fun renderDialog(dialogService: DialogService) {}

}
package com.ilonacodes.maze

val ACCELERATION_RATIO = 60.0f
val VELOCITY_RATIO = ACCELERATION_RATIO / 100.0f

interface CappableVector {
    val max: Float
    var x: Float
    var y: Float

    fun cap() {
        val size = Math.sqrt((x * x + y * y).toDouble()).toFloat()

        if (size > max) {
            x = x / size * max
            y = y / size * max
        }
    }
}
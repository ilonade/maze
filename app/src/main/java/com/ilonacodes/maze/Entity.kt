package com.ilonacodes.maze

class Entity {

    val components = mutableListOf<Component?>()

    init {
        entities.add(this)
    }

    inline fun <reified T : Component> get(): T? {
        components.forEach { component ->
            if (component is T?) {
                return component
            }
        }

        return null
    }

    inline fun <reified T : Component> set(value: T) {
        val componentStorage = Components.lookup<T>()
        value.componentStorage = componentStorage
        componentStorage.add(value)

        value.entity = this

        components.forEachIndexed { index, component ->
            if (component is T?) {
                if (component != null) {
                    removeAt<T>(index)
                }

                components[index] = value
                return
            }
        }

        components.add(value)
    }

    inline fun <reified T : Component> remove() {
        components.forEachIndexed { index, component ->
            if (component is T) {
                removeAt<T>(index)
            }
        }
    }

    inline fun <reified T : Component> removeAt(index: Int) {
        val component = components[index] as? T ?: return
        val componentsStorage = component.componentStorage as? Components<T> ?: return

        componentsStorage.remove(component)
    }

    fun removeEntity() {
        components.forEach { it?.remove() }
        entities.remove(this)
    }

    companion object {
        val entities = mutableListOf<Entity>()

        fun create(block: Entity.() -> Unit) {
            Entity().apply(block)
        }
    }

}
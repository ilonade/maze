package com.ilonacodes.maze

import com.ilonacodes.maze.components.Dialog
import com.ilonacodes.maze.components.DialogButton
import com.ilonacodes.maze.dialog.DialogService
import com.ilonacodes.maze.levels.levelOne
import com.ilonacodes.maze.renderer.Renderer
import com.ilonacodes.maze.systems.*

class Game(private val gameView: GameView,
           private val gameActivity: GameActivity,
           private val systems: List<System>) {

    fun mainLoop() {
        levelOne.create()

        while (true) {
            main()
            Thread.sleep(16);
        }
    }

    private val renderer = Renderer()
    private val dialogService = DialogService(this)

    private fun main() {
        if (!dialogService.hasDialogsActive()) {
            systems.forEach { it.update() }
            systems.forEach { it.render(renderer) }
            systems.forEach { it.renderDialog(dialogService) }

            renderer.flush(gameView)
            dialogService.flush(gameActivity)
        }
    }

    fun updateAccelerometer(x: Float, y: Float, z: Float) {
        systems.forEach { it.updateAccelerometer(x, y, z) }
    }

    fun activateDialogButtonAction(dialog: Dialog, button: DialogButton) {
        systems.forEach { it.activateDialogButtonAction(dialog, button) }
    }

}

fun defaultGame(fullscreen_content: GameView, gameActivity: GameActivity) =
        Game(fullscreen_content, gameActivity, listOf(
                // Controls
                AccelerometerControlSystem(),

                // Physics
                ForceSystem(),
                AccelerationSystem(),
                VelocitySystem(),
                VelocityResistanceSystem(),
                SimpleCollisionSystem(),

                // Victory & Defeat
                DefeatSystem(),
                VictorySystem(),

                // Levels
                RetryLevelSystem(),
                AdvanceToNextLevelSystem(),
                NewGameSystem(),

                // Rendering
                ViewPortSystem(),
                RectangleShapeRenderingSystem(),
                CircleShapeRenderingSystem(),

                // Dialogs
                DialogSystem(),

                // Debug
                AccelerometerDebugSystem(),
                ArrowRenderingSystem()
        ))


package com.ilonacodes.maze.renderer.commands

import android.graphics.Canvas
import android.graphics.Paint
import com.ilonacodes.maze.renderer.RenderCommand

class Line(private val startX: Float,
           private val startY: Float,
           private val endX: Float,
           private val endY: Float,
           color: Long) : RenderCommand {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.color = color.toInt()
    }

    override fun renderTo(canvas: Canvas) {
        val originX = canvas.width / 2
        val originY = canvas.height / 2

        canvas.drawLine(
                originX + startX,
                originY + startY,
                originX + endX,
                originY + endY,
                paint)
    }

}
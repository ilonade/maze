package com.ilonacodes.maze.renderer

import android.graphics.Canvas

interface RenderCommand {
    fun renderTo(canvas: Canvas)
}
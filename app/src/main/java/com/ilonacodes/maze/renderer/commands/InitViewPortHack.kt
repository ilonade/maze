package com.ilonacodes.maze.renderer.commands

import android.graphics.Canvas
import com.ilonacodes.maze.components.ViewPort
import com.ilonacodes.maze.renderer.RenderCommand

class InitViewPortHack : RenderCommand {

    override fun renderTo(canvas: Canvas) {
        val viewPort = ViewPort.instance
        viewPort.screenWidth = canvas.width * 1f
        viewPort.screenHeight = canvas.height * 1f
    }

}
package com.ilonacodes.maze.renderer

import android.graphics.Canvas
import com.ilonacodes.maze.GameView

class Renderer {

    private val commands = mutableListOf<RenderCommand>()

    fun flush(gameView: GameView) {
        gameView.renderer = this
        gameView.tryDraw()
    }

    fun renderTo(canvas: Canvas) {
        synchronized(commands) {
            commands.forEach { it.renderTo(canvas) }
            commands.clear()
        }
    }

    fun add(command: RenderCommand) {
        synchronized(commands) {
            commands.add(command)
        }
    }

}
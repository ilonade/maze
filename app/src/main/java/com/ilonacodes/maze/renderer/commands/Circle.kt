package com.ilonacodes.maze.renderer.commands

import android.graphics.Canvas
import android.graphics.Paint
import com.ilonacodes.maze.renderer.RenderCommand

class Circle(private val x: Float,
             private val y: Float,
             private val radius: Float,
             color: Long) : RenderCommand {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.style = Paint.Style.FILL
        paint.color = color.toInt()
    }

    override fun renderTo(canvas: Canvas) {
        canvas.drawCircle(x, y, radius, paint)
    }

}
package com.ilonacodes.maze.renderer.commands

import android.graphics.Canvas
import android.graphics.Paint
import com.ilonacodes.maze.renderer.RenderCommand

class Rectangle(private val x: Float,
                private val y: Float,
                private val width: Float,
                private val height: Float,
                color: Long) : RenderCommand {

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    init {
        paint.style = Paint.Style.FILL
        paint.color = color.toInt()
    }

    override fun renderTo(canvas: Canvas) {
        canvas.drawRect(x, y, x + width, y + height, paint)
    }

}
package com.ilonacodes.maze

class Components<T : Component>(val sample: T) {

    private val storage = mutableListOf<T>()

    init {
        componentStorages.add(this)
    }

    fun forEach(action: (T) -> Unit) {
        storage.forEach(action)
    }

    fun first() = storage.firstOrNull()

    fun firstEntity() = first()?.entity

    fun clearEntities() = asList().forEach { it.entity?.removeEntity() }

    fun forEachEntity(action: (Entity) -> Unit) {
        forEach {
            val entity = it.entity ?: return@forEach
            action(entity)
        }
    }

    fun asList(): List<T> {
        val list = mutableListOf<T>()
        forEach { list.add(it) }
        return list
    }

    fun isEmpty() = asList().isEmpty()

    operator fun get(index: Int): T {
        return storage[index]
    }

    fun add(component: T) {
        storage.add(component)
        component.componentStorage = this
    }

    fun remove(component: T) {
        component.componentStorage = null
        storage.remove(component)
    }

    companion object {
        val componentStorages = mutableListOf<Components<*>>()

        inline fun <reified T : Component> lookup(): Components<T> {
            componentStorages.forEach { components ->
                if (components.sample is T) {
                    return components as Components<T>
                }
            }

            val regex = Regex("""^class ([\w.]+).*$""")
            val componentNameString = "${T::class}"
            val matches = regex.matchEntire(componentNameString)
            val fullName = matches?.groupValues?.get(1) ?: componentNameString
            val componentName = fullName.split(".").last()
            val variableName = "${componentName.decapitalize()}s"

            throw RuntimeException("""
                |Unable to find Components<$componentName>
                |Please define the following companion object on component:
                |   companion object {
                |       val $variableName = Components($componentName())
                |   }
            """.trimMargin())
        }
    }

}

package com.ilonacodes.maze

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import com.ilonacodes.maze.renderer.Renderer

class GameView : SurfaceView, SurfaceHolder.Callback {

    override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {}

    override fun surfaceDestroyed(holder: SurfaceHolder?) {}

    override fun surfaceCreated(holder: SurfaceHolder?) {}

    constructor(context: Context?)
            : super(context)

    constructor(context: Context?, attrs: AttributeSet)
            : super(context, attrs)

    constructor(context: Context?, attrs: AttributeSet, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr)

    lateinit var renderer: Renderer

    fun tryDraw() {
        val canvas = holder.lockCanvas() ?: return

        canvas.drawColor(Color.BLACK)
        renderer.renderTo(canvas)

        holder.unlockCanvasAndPost(canvas)
    }

}
package com.ilonacodes.maze.dialog

import android.app.Activity
import android.app.AlertDialog
import com.ilonacodes.maze.Game
import com.ilonacodes.maze.components.Dialog
import com.ilonacodes.maze.components.DialogButton

class DialogService(private val game: Game) {

    private val queue = mutableListOf<Dialog>()
    private var dialogsShown = 0

    fun addDialog(dialog: Dialog) = synchronized(queue) {
        queue.add(dialog)
    }

    fun flush(activity: Activity) = synchronized(queue) {
        queue.forEach { dialog ->
            renderDialog(activity, dialog)
        }
        queue.clear()
    }

    fun hasDialogsActive() = dialogsShown > 0

    private fun renderDialog(activity: Activity, dialog: Dialog) {
        activity.runOnUiThread {
            dialogsShown++

            AlertDialog.Builder(activity)
                    .setTitle(dialog.title)
                    .setMessage(dialog.text)
                    .setButtonActions(dialog)
                    .create()
                    .setFirstButtonAsDismissAction(dialog)
                    .show()
        }
    }

    private fun AlertDialog.Builder.setButtonActions(dialog: Dialog): AlertDialog.Builder {
        if (dialog.buttons.isNotEmpty()) {
            setPositiveButton(dialog, dialog.buttons[0])
        }

        if (dialog.buttons.size > 1) {
            setNegativeButton(dialog, dialog.buttons[1])
        }

        return this
    }

    private fun AlertDialog.Builder.setPositiveButton(dialog: Dialog, button: DialogButton) {
        setPositiveButton(button.title) { _, _ ->
            activateDialogButtonAction(dialog, button)
        }
    }

    private fun AlertDialog.Builder.setNegativeButton(dialog: Dialog, button: DialogButton) {
        setNegativeButton(button.title) { _, _ ->
            activateDialogButtonAction(dialog, button)
        }
    }

    private fun AlertDialog.setFirstButtonAsDismissAction(dialog: Dialog): AlertDialog {
        dialog.buttons.firstOrNull()?.let { button ->
            setOnDismissListener {
                activateDialogButtonAction(dialog, button)
            }
        }

        return this
    }

    private fun activateDialogButtonAction(
            dialog: Dialog,
            button: DialogButton) = synchronized(dialog.done) {

        if (dialog.done) return

        game.activateDialogButtonAction(dialog, button)
        dialogsShown--

        dialog.done = true

    }

}
package com.ilonacodes.maze

abstract class Component {
    var entity: Entity? = null
    var componentStorage: Components<*>? = null

    fun remove() {
        (componentStorage as? Components<Component>)?.remove(this)
    }
}
package com.ilonacodes.maze.components

import com.ilonacodes.maze.CappableVector
import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Force(override var x: Float,
                 override var y: Float,
                 override val max: Float) : Component(), CappableVector {
    companion object {
        val forces = Components(Force(0f, 0f, 0f))
    }
}

fun Entity.force(x: Float, y: Float, max: Float) =
        set(Force(x, y, max))
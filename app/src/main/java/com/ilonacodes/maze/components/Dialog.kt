package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Dialog(val title: String,
                  val text: String,
                  val buttons: List<DialogButton>,
                  var done: Boolean = false) : Component() {

    companion object {
        val dialogs = Components(Dialog("", "", emptyList()))
    }
}

data class DialogButton(val title: String,
                        val action: Entity.() -> Unit)

fun Entity.dialog(title: String,
                  text: String,
                  buttons: List<DialogButton>) =
        set(Dialog(title, text, buttons))
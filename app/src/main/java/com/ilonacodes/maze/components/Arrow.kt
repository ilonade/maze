package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Arrow(var startX: Float,
                 var startY: Float,
                 var endX: Float,
                 var endY: Float,
                 var color: Long) : Component() {
    companion object {
        val arrows = Components(Arrow(0f, 0f, 0f, 0f, 0))
    }
}

fun Entity.arrow(startX: Float,
                 startY: Float,
                 endX: Float,
                 endY: Float,
                 color: Long) =
        set(Arrow(startX, startY, endX, endY, color))
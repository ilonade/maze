package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Position(var x: Float, var y: Float) : Component() {
    companion object {
        val positions = Components(Position(0f, 0f))
    }
}

fun Entity.position(x: Float, y: Float) = set(Position(x, y))
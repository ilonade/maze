package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Name(val name: String): Component() {
    companion object {
        val names = Components(Name(""))
    }
}

fun Entity.name(name: String) = set(Name(name))
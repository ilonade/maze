package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class Victory(val reason: String) : Component() {
    companion object {
        val victories = Components(Victory(""))
    }
}

fun Entity.victory(reason: String) = set(Victory(reason))
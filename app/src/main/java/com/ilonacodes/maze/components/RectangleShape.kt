package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class RectangleShape(val width: Float, val height: Float) : Component() {
    companion object {
        val rectangleShapes = Components(RectangleShape(0f, 0f))
    }
}

fun Entity.rectangleShape(width: Float, height: Float) =
        set(RectangleShape(width, height))
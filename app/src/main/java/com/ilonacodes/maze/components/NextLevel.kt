package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.levels.LevelCreator
import com.ilonacodes.maze.levels.NoLevel

data class NextLevel(val creator: LevelCreator) : Component() {
    companion object {
        val nextLevels = Components(NextLevel(NoLevel()))
    }
}

fun Entity.nextLevel(creator: LevelCreator) =
        set(NextLevel(creator))
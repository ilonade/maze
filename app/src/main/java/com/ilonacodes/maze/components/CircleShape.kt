package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class CircleShape(val radius: Float) : Component() {
    companion object {
        val circleShapes = Components(CircleShape(0f))
    }
}

fun Entity.circleShape(radius: Float) = set(CircleShape(radius))
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class ViewPort(var x: Float,
                    var y: Float,
                    var width: Float,
                    var height: Float,
                    var screenWidth: Float = 100f,
                    var screenHeight: Float = 100f,
                    var originX: Float = 0.0f,
                    var originY: Float = 0.0f,
                    var scale: Float = 1.0f) : Component() {

    companion object {

        private val default = ViewPort(0f, 0f, 0f, 0f)
        val viewPorts = Components(default)

        val instance: ViewPort
            get() {
                var viewPort = ViewPort.default
                viewPorts.forEach { viewPort = it }
                return viewPort
            }
    }
}

fun Entity.viewPort(x: Float,
                    y: Float,
                    width: Float,
                    height: Float) =
        set(ViewPort(x, y, width, height))
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.levels.LevelCreator
import com.ilonacodes.maze.levels.NoLevel

data class FinalLevel(val newGame: LevelCreator) : Component() {
    companion object {
        val finalLevels = Components(FinalLevel(NoLevel()))
    }
}

fun Entity.finalLevel(newGame: LevelCreator) =
        set(FinalLevel(newGame))
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class Defeat(val reason: String) : Component() {
    companion object {
        val defeats = Components(Defeat(""))
    }
}

fun Entity.defeat(reason: String) = set(Defeat(reason))
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class BounceOnCollision : Component() {
    companion object {
        val bouncesOnCollision = Components(BounceOnCollision())
    }
}

fun Entity.bounceOnCollision() = set(BounceOnCollision())
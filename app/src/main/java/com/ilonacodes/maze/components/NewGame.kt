package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class NewGame : Component() {
    companion object {
        val newGames = Components(NewGame())
    }
}

fun Entity.newGame() =
        set(NewGame())
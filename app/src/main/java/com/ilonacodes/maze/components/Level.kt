package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.levels.LevelCreator
import com.ilonacodes.maze.levels.NoLevel

data class Level(val creator: LevelCreator) : Component() {
    companion object {
        val levels = Components(Level(NoLevel()))
    }
}

fun Entity.level(creator: LevelCreator) =
        set(Level(creator))
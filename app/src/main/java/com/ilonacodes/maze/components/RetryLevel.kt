package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class RetryLevel : Component() {
    companion object {
        val retryLevels = Components(RetryLevel())
    }
}

fun Entity.retryLevel() = set(RetryLevel())
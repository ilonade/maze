package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class AdvanceToNextLevel : Component() {
    companion object {
        val advancesToNextLevel = Components(AdvanceToNextLevel())
    }
}

fun Entity.advanceToNextLevel() = set(AdvanceToNextLevel())
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Mass(val mass: Float): Component() {
    companion object {
        val masses = Components(Mass(0f))
    }
}

fun Entity.mass(mass: Float) = set(Mass(mass))
package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class MarkOnCollision(val mark: (Entity.() -> Unit)? = null) : Component() {

    companion object {
        val marksOnCollision = Components(MarkOnCollision())
    }

}

fun Entity.markOnCollision(mark: (Entity.() -> Unit)? = null) =
        set(MarkOnCollision(mark))
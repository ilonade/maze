package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class AccelerometerControl: Component() {
    companion object {
        val accelerometerControls = Components(AccelerometerControl())
    }
}

fun Entity.accelerometerControl() = set(AccelerometerControl())
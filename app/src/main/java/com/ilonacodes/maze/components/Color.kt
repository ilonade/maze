package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

data class Color(val color: Long) : Component() {
    companion object {
        val colors = Components(Color(0))
    }
}

fun Entity.color(color: Long) = set(Color(color))
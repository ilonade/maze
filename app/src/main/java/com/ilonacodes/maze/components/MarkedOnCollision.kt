package com.ilonacodes.maze.components

import com.ilonacodes.maze.Component
import com.ilonacodes.maze.Components
import com.ilonacodes.maze.Entity

class MarkedOnCollision : Component() {
    companion object {
        val markedsOnCollision = Components(MarkedOnCollision())
    }
}

fun Entity.markedOnCollision() =
        set(MarkedOnCollision())
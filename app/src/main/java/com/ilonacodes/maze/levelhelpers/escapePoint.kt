package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.components.*

private var escapePointCounter = 1

fun escapePoint(pos: Pair<Int, Int>,
                radius: Float = 3.5f) {
    
    Entity.create {
        name("escape point $escapePointCounter")
        color(0xff11d311)
        position(pos.first * 1.0f + radius, pos.second * 1.0f + radius)
        circleShape(radius)
        markOnCollision { set(Victory("Ball has escaped the maze! :)")) }
    }

    escapePointCounter++
}
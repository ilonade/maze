package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.components.*
import com.ilonacodes.maze.levels.LevelCreator

fun createLevel(name: String,
                self: LevelCreator,
                next: LevelCreator? = null,
                newGame: LevelCreator? = null,
                size: Pair<Int, Int>) {

    val width = size.first * 1.0f
    val height = size.second * 1.0f

    clearAllEntities()

    Entity.create {
        name(name)
        level(self)
        next?.let { nextLevel(next) }
        newGame?.let { finalLevel(newGame) }
    }

    Entity.create {
        name("view port")
        viewPort(0.0f, 0.0f, width, height)
    }

    Entity.create {
        name("map")
        color(0xff8d8882)
        position(0.0f, 0.0f)
        rectangleShape(width, height)
    }

    Entity.create {
        name("boundary (left-top -> right-top)")
        color(0xff000000)
        position(-5.0f, -5.0f)
        rectangleShape(width + 10.0f, 5.0f)
        bounceOnCollision()
    }

    Entity.create {
        name("boundary (left-top -> left-bottom)")
        color(0xff000000)
        position(-5.0f, -5.0f)
        rectangleShape(5.0f, height + 10.0f)
        bounceOnCollision()
    }

    Entity.create {
        name("boundary (left-bottom -> right-bottom)")
        color(0xff000000)
        position(-5.0f, height)
        rectangleShape(width + 10.0f, 5.0f)
        bounceOnCollision()
    }

    Entity.create {
        name("boundary (right-top -> right-bottom)")
        color(0xff000000)
        position(width, -5.0f)
        rectangleShape(5.0f, height + 10.0f)
        bounceOnCollision()
    }

}
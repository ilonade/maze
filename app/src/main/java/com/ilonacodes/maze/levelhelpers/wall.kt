package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.components.*

private var wallCounter = 0

fun wall(pos: Pair<Int, Int>,
         size: Pair<Int, Int>) {

    Entity.create {
        name("wall $wallCounter")
        color(0xff291c11)
        position(pos.first * 1f, pos.second * 1f)
        rectangleShape(size.first * 1f, size.second * 1f)
        bounceOnCollision()
    }

    wallCounter++

}
package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity

fun clearAllEntities() {
    Entity.entities.toList().forEach { it.removeEntity() }
}
package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.components.*

private var holeCounter = 1

fun hole(pos: Pair<Int, Int>,
         radius: Float = 3.5f) {

    Entity.create {
        name("hole $holeCounter")
        color(0xff121212)
        position(pos.first * 1.0f + radius, pos.second * 1.0f + radius)
        circleShape(radius)
        markOnCollision { set(Defeat("Ball has fallen in the hole :(")) }
    }

    holeCounter++
}
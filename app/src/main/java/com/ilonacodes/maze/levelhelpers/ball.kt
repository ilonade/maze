package com.ilonacodes.maze.levelhelpers

import com.ilonacodes.maze.Entity
import com.ilonacodes.maze.components.*

fun ball(pos: Pair<Int, Int>,
         radius: Float = 2.5f) {

    Entity.create {
        name("ball")
        color(0xffe5e5e5)
        position(pos.first * 1.0f + radius, pos.second * 1.0f + radius)
        circleShape(radius)
        velocity(0.0f, 0.0f, max = 30.0f)
        acceleration(0.0f, 0.0f, max = 10.0f)
        mass(0.1f)
        force(0.0f, 0.0f, max = 100.0f)
        accelerometerControl()
        bounceOnCollision()
        markedOnCollision()
    }
    
}
package com.ilonacodes.maze.levels

import com.ilonacodes.maze.levelhelpers.*

class LevelThree : LevelCreator {
    override fun create() {
        createLevel(
                name = "Level 3",
                self = this,
                newGame = levelOne,
                size = 70 x 70
        )

        // h . . . . . . w w w w w w w
        // . . h h . . h . . . h . . .
        // . w w w w . . . h . . . h .
        // . . . E w h h w w w w w w .
        // w h . w w . h . . . . . . .
        // w . . w . . . w w w w . w .
        // w . h w . . . . . . w . w .
        // w . . w . w . w h . w . w .
        // w h . w . w . w . . w . w .
        // w . h w . . . w . h w . w .
        // w . h w . w . w . . w . w .
        // w h . w . w . w h . . . . .
        // w . h w . w . w w w w . w .
        // . . . . . . b . . . . h w .

        ball(30 x 65, radius = 2f)

        escapePoint(15 x 15)

        hole(0 x 0)
        hole(10 x 5)
        hole(15 x 5)
        hole(30 x 5)
        hole(50 x 5)
        hole(40 x 10)
        hole(60 x 10)
        hole(25 x 15)
        hole(30 x 15)
        hole(5 x 20)
        hole(30 x 20)
        hole(10 x 30)
        hole(40 x 35)
        hole(5 x 40)
        hole(10 x 45)
        hole(45 x 45)
        hole(10 x 50)
        hole(5 x 55)
        hole(40 x 55)
        hole(10 x 60)
        hole(55 x 65)

        // vertical walls
        wall(pos = 0 x 20, size = 5 x 45)
        wall(pos = 15 x 20, size = 5 x 45)
        wall(pos = 20 x 10, size = 5 x 15)
        wall(pos = 25 x 35, size = 5 x 10)
        wall(pos = 25 x 50, size = 5 x 15)
        wall(pos = 35 x 35, size = 5 x 30)
        wall(pos = 50 x 25, size = 5 x 30)
        wall(pos = 60 x 25, size = 5 x 30)
        wall(pos = 60 x 60, size = 5 x 10)

        // horizontal walls
        wall(pos = 5 x 10, size = 15 x 5)
        wall(pos = 35 x 0, size = 35 x 5)
        wall(pos = 35 x 15, size = 30 x 5)
        wall(pos = 35 x 25, size = 15 x 5)
        wall(pos = 40 x 60, size = 15 x 5)
    }
}

val levelThree = LevelThree()
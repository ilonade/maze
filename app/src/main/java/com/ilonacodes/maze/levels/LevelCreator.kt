package com.ilonacodes.maze.levels

interface LevelCreator {
    fun create()
}

class NoLevel : LevelCreator {
    override fun create() {

    }
}
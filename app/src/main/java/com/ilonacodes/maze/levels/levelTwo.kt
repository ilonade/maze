package com.ilonacodes.maze.levels

import com.ilonacodes.maze.levelhelpers.*

class LevelTwo : LevelCreator {
    override fun create() {
        createLevel(
                name = "Level 2",
                self = this,
                next = levelThree,
                size = 100 x 100
        )

        // . . . . w . . . . . . . . . . . . . . .
        // . . . . w . . . . . . . . . w . . . . .
        // . . . . w h h h . . h h h . w . . h . .
        // . . . . w . . . . . . . . . w . . . . .
        // . . . . w . . . h . . . . . w . . . . .
        // . . . . w . . . . h . . . . w h . . . .
        // . . . . w . . . . . . . h . w . . . . .
        // . . . . w . . . . . . . . . w . . . . .
        // . . . . w . . . . . . . . . w . . . . .
        // . . . . w . . . . . . . . . w h . . . .
        // . . . . w . . . h h . . . . w . . . . .
        // . . . . w . . . h h . . . . w . . . . .
        // . . . . w . . . . . . . . . w . . h . .
        // . . . . w . . . . . . . . . w . . . . .
        // . h . . . . . . . . . . . . w . . . . .
        // . . h . . . . . . . . . . . w . . . . .
        // . . . . w . . h . . . h . . w . . . . .
        // . . . . w . . . . . . . . . w . . b . .
        // E . . . w . . . . . . . . . w h . . . .
        // . . . . w . . . . . . . . . . . . . . .

        ball(95 x 95)

        wall(pos = 20 x 0, size = 5 x 70)
        wall(pos = 20 x 80, size = 5 x 20)
        wall(pos = 70 x 7, size = 5 x 86)

        hole(80 x 90)
        hole(90 x 60)
        hole(80 x 45)
        hole(80 x 25)
        hole(90 x 10)

        hole(60 x 80)
        hole(40 x 80)

        hole(45 x 50)
        hole(50 x 50)
        hole(45 x 55)
        hole(50 x 55)

        hole(65 x 30)

        hole(45 x 20)
        hole(50 x 25)

        hole(30 x 10)
        hole(35 x 10)
        hole(40 x 10)

        hole(55 x 10)
        hole(60 x 10)
        hole(65 x 10)

        hole(5 x 70)
        hole(10 x 75)

        escapePoint(3 x 90)
    }
}

val levelTwo = LevelTwo()
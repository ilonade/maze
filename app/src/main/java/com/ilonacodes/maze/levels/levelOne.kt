package com.ilonacodes.maze.levels

import com.ilonacodes.maze.levelhelpers.*

class LevelOne : LevelCreator {
    override fun create() {
        createLevel(
                name = "Level 1",
                self = levelOne,
                next = levelTwo,
                size = 40 x 40
        )

        // b w . . . w . .
        // . w . w . w . .
        // . w . w . w . .
        // . w . w . . . .
        // . w . w h w . .
        // . w . w . w . .
        // . . . w . w . E
        // . w . . . w . .

        ball(0 x 0, radius = 2f)

        escapePoint(35 x 30, radius = 2.5f)

        hole(20 x 20, radius = 2.5f)

        wall(pos = 5 x 0, size = 5 x 30)
        wall(pos = 5 x 35, size = 5 x 5)
        wall(pos = 15 x 5, size = 5 x 30)
        wall(pos = 25 x 0, size = 5 x 15)
        wall(pos = 25 x 20, size = 5 x 20)
    }
}

val levelOne = LevelOne()